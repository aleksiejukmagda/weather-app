const wDay = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
const wMonth = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];


const iconValue = {
    CLEARDAY: '01d',
    CLEARNIGHT: '01n',
    RAIN: '10d',
    SNOW: '13d',
    FOG: '50d',
    CLOUDLY: '03d',
    THUNDER: '11d',
    PARTLY_CLOUDLY_DAY: '02d',
    PARTLY_CLOUDLY_NIGHT: '02n'
}

// fetch the weather
function fetchWeatherReport(apiKey, latitude, longitude) {
    var OwApiLink = `https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=${apiKey}&units=metric`;
    var OwApiLinkDaily = `https://api.openweathermap.org/data/2.5/onecall?lat=${latitude}&lon=${longitude}&exclude={current,minutely,alerts}&appid=${apiKey}&units=metric`;

    fetch(OwApiLink)
    .then(response => {
        return response.json()
    })
    .then(data => {
        // Work with JSON data here
        var summary = data.weather[0].description.toUpperCase();
        var temperature = data.main.temp;
        var icon = data.weather[0].icon;
        var presure = data.main.pressure;
        var humidity = data.main.humidity;
        var windSpeed = data.wind.speed;
        var ts = new Date(data.dt * 1000);
        var forecastDate = `${wDay[ts.getDay()]} ${wMonth[ts.getMonth()]} ${ts.getDate()}`;
        var city = data.name;


        //Set values for the current conditions
        document.getElementById("location").innerHTML = city;
        document.getElementById("dayTime").innerHTML = forecastDate;
        document.getElementById("summary").innerHTML = summary;
        document.getElementById("currentTemp").innerHTML = `${Math.round(temperature)}&deg`;
        document.getElementById("weatherIcon").src = getICON('icon');
        document.getElementById("pressure").innerHTML = `Pressure: ${presure} mb`;
        document.getElementById("humidity").innerHTML = `Humidity: ${humidity}%`;
        document.getElementById("wind").innerHTML = `Wind: ${windSpeed} mps`;
    })
    .catch(err => {
        throw(`Sorry, an error occured. ${err}`);
    })

    fetch(OwApiLinkDaily)
    .then(response => {
        return response.json()
    })
    .then(data => {
        console.log(data.daily)
        //render the forcasts tabs
        document.getElementById("weeklyForecast").innerHTML = renderWeeklyForecast(data.daily);
        document.getElementById("dailyForecast").innerHTML = renderDailyForecast(data.hourly);
    })
    .catch(err => {
        throw(`Sorry, an error occured. ${err}`);
    })
}

//find the lst and long of the users location 
function initGeolocation() {
    if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(success, fail)
    } else {
        alert('Sorry, your browser does not support geolocation services.');
    }
}

//if naviation is available show weather for the current location
function success(position) {
    fetchWeatherReport(owKey, position.coords.latitude, position.coords.longitude);
}

function fail() {
    alert('Sorry, your browser does not support geolocation services.');
}

//render the weekly forecast
function renderWeeklyForecast(fcData) {
    let resultsHTML = '<tr><th>Day</th><th class="color-coral">Conditions</th><th>Hi</th><th>Lo</th></tr>';

    let rowCount = fcData.length;

    for (let i=0; i<rowCount; i++) {
        let ts = new Date(fcData[i].dt * 1000);
        let dayTime = wDay[ts.getDay()];
        let summary = fcData[i].weather[0].description;
        let tempHigh = `${Math.round(fcData[i].temp.max)}&deg`;
        let tempLow = `${Math.round(fcData[i].temp.min)}&deg`;

        resultsHTML += renderRow(dayTime, summary, tempHigh, tempLow);
    }

    return resultsHTML;
}

//render the daily forecast
function renderDailyForecast(fcData) {
    let resultsHTML = '<tr><th>Time</th><th class="color-coral">Conditions</th><th>Temp</th><th>Precip</th></tr>';

    let rowCount = fcData.length;
    if (rowCount > 8) {
        rowCount = 8;
    }

    for (let i=0; i<rowCount; i++) {
        let ts = new Date(fcData[i].dt * 1000);

        let hours = ts.getHours();
        if (hours > 0 && hours <= 12) {
            timeValue = "" + hours;
        } else if (hours > 12) {
            timeValue = "" + (hours - 12);
        } else if (hours == 0) {
            timeValue = "12";
        }
        timeValue += (hours >= 12) ? " PM" : " AM"; // get AM/PM

        let summary = fcData[i].weather[0].description;
        let tempHigh = `${Math.round(fcData[i].temp)}&deg`;
        let precipProbability = `${(fcData[i].pop)*100}%`;

        resultsHTML += renderRow(timeValue, summary, tempHigh, precipProbability);
    }

    return resultsHTML;
}

//template function to render grid colums
function renderRow(dayTime, summary, tempHigh, tempLow) {
return `<tr><td>${dayTime}</td><td class="color-coral">${summary}</td><td>${tempHigh}</td><td>${tempLow}</td></tr>`
}

//render the correct icon
function getICON(icon) {
    switch (icon) {
        case iconValue.CLEARDAY:
            return "/images/SunnyDay.png";
        case iconValue.CLOUDY:
            return "/images/Cloudy.png";
        case iconValue.PARTLY_CLOUDY_DAY:
            return "/images/Fog.png";
        case iconValue.CLEARNIGHT:
            return "/images/ClearMoon.png";
        case iconValue.FOG:
            return "/images/ClearMoon.png";
        case iconValue.PARTLY_CLOUDY_NIGHT:
            return "i/mages/CloudyMoon.png";
        case iconValue.RAIN:
            return "/images/Rain.png";
        case iconValue.THUNDER:
            return "/images/Thunder.png";
        case iconValue.SNOW:
            return "/images/SNOW.png";
        default:
            return "images/SunnyDay.png";
    }
}

